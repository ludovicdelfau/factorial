open import Data.List
open import Data.Nat
open import Data.Nat.Tactic.RingSolver
open import Relation.Binary.PropositionalEquality
open Relation.Binary.PropositionalEquality.≡-Reasoning

infix 8 _!ᵣ

_!ᵣ : ℕ → ℕ
_!ᵣ zero = 1
_!ᵣ (suc n) = (1 + n) * n !ᵣ

infix 8 _!ₜᵣ

!ₜᵣ-aux : ℕ → ℕ → ℕ
!ₜᵣ-aux zero a = a
!ₜᵣ-aux (suc n) a = !ₜᵣ-aux n (a * (1 + n))

_!ₜᵣ : ℕ → ℕ
_!ₜᵣ n = !ₜᵣ-aux n 1

!ₜᵣ-aux-lemma : ∀ n o p → !ₜᵣ-aux n (o * p) ≡ p * !ₜᵣ-aux n o
!ₜᵣ-aux-lemma zero o p =
    begin
    !ₜᵣ-aux 0 (o * p)
    ≡⟨⟩
    o * p
    ≡⟨ solve (o ∷ p ∷ []) ⟩
    p * o
    ≡⟨⟩
    p * !ₜᵣ-aux 0 o
    ∎
!ₜᵣ-aux-lemma (suc n) o p =
    begin
    !ₜᵣ-aux (1 + n) (o * p)
    ≡⟨⟩
    !ₜᵣ-aux n (o * p * (1 + n))
    ≡⟨ cong (λ a → !ₜᵣ-aux n a) (solve (n ∷ o ∷ p ∷ [])) ⟩
    !ₜᵣ-aux n (o * (1 + n) * p)
    ≡⟨ !ₜᵣ-aux-lemma n (o * (1 + n)) p ⟩
    p * !ₜᵣ-aux n (o * (1 + n))
    ≡⟨⟩
    p * !ₜᵣ-aux (1 + n) o
    ∎

n!ₜᵣ≡n!ᵣ : ∀ n → n !ₜᵣ ≡ n !ᵣ
n!ₜᵣ≡n!ᵣ zero = refl
n!ₜᵣ≡n!ᵣ (suc n) =
    begin
    (1 + n) !ₜᵣ
    ≡⟨⟩
    !ₜᵣ-aux (1 + n) 1
    ≡⟨⟩
    !ₜᵣ-aux n (1 * (1 + n))
    ≡⟨ !ₜᵣ-aux-lemma n 1 (1 + n) ⟩
    (1 + n) * !ₜᵣ-aux n 1
    ≡⟨⟩
    (1 + n) * n !ₜᵣ
    ≡⟨ cong (λ n! → (1 + n) * n!) (n!ₜᵣ≡n!ᵣ n) ⟩
    (1 + n) * n !ᵣ
    ≡⟨⟩
    (1 + n) !ᵣ
    ∎
