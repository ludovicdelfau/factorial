# Factorial

Proving with Agda that a recursive and tail recursive definitions
of the factorial function are equivalent.

The proof can be found [here](https://factorial-ludovicdelfau-cd96b5b20d28aa75c16c1ae6ee7fe47ff1d4d9c.gitlab.io/Factorial.html).
